import {useState, useEffect} from 'react'; 

import {POPULAR_BASE_URL} from '../../config'; 

export const useHomeFetch = () => {
    const  [state, setState] = useState({movies: []});
    const [loading, setLoading] = useState(false); 
    const [error, setError] = useState(false); 

    console.log(state);

    const fetchMovies = async endpoint => {
        setError(false); 
        setLoading(true);

        const isLoadMore = endpoint.search('page');


        try {
            const res = await (await fetch(endpoint)).json();
            setState(prev => ({
                ...prev, 
                movies: 
                isLoadMore !== -1 
                ?[...prev.movies, ...res.results]
                :[...res.results],
                heroImage: prev.heroImage || res.results[0], 
                currentPage: res.page, 
                totalPages: res.total_pages,
            }));

        } catch(err) {
            setError(true);
        }
        setLoading(false);
    }

    useEffect(() => {
        fetchMovies(`${POPULAR_BASE_URL}`);
    }, [])

    return [{state, loading, error}, fetchMovies]; 
}