import React from 'react';
import styled from 'styled-components';

import TMDBLogo from '../images/tmdb_logo.svg';

const StyledHeader = styled.div`
    background: #1c1c1c; 
    padding: 0 20px;
    box-sizing: border-box;

    .header-content{
        max-width: 1280px;
        max-height: 120px;
        padding: 20px 0; 
        margin: 0 auto; 
        box-sizing: border-box;

        @media screen and (max-width: 500px) {
            min-height: 0px;
        }
    }
`;

const StyledLogo = styled.img`
    width: 250px; 
    margin-top: 20px;
    height: 80px;
   
    @media screen and (max-width: 500px) {
        width: 150px;
        margin-top: 5px;
        height: 40px;
    }
`


const Header = () => 
(
    <StyledHeader>
        <div className="header-content">
            <StyledLogo src={TMDBLogo} alt='rmdb-logo' />
        </div>
    </StyledHeader>
)
export default Header;