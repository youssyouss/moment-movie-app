import React from 'react';
import { StyledMovieThumb } from '../styles/StyledMovieThumb';
import { useModal } from '../hooks/useModal';
import Modal from './Modal';


const MovieThumb = ({image, clickable}) => {
    const {isShowing, toggle} = useModal();

   return (
   <StyledMovieThumb>
        { clickable
            ?(
            <div>
                <button onClick={toggle}>
                <img  className="clickable" src={image} alt="movie-thumbnail"/>
                </button>
                <Modal
                isShowing={isShowing}
                hide={toggle} 
                />
            </div>
         
            )
            : ( <img src={image} alt="movie-thumbnail"/> )
        }
    </StyledMovieThumb>
    )
}


export default MovieThumb;