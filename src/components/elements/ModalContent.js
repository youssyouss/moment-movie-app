import React from 'react';

import NoImage from '../images/no_image.jpg';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../../config';

import MovieThumb from './MovieThumb';
import Spinner from './Spinner';


//custom hook

import { useMovieFetch } from '../hooks/useMovieFetch'

const ModalContent = ({movieId}) => {
    const [movie, loading, error] = useMovieFetch(movieId);

    if (error) return <div>Something went wrong ...</div>;
    if (loading) return <Spinner/>;

    return (
        <div>
        <MovieThumb
          image={
            movie.poster_path
              ? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}`
              : NoImage
          }
          clickable={false}
          alt="moviethumb"
        />
         <div className="movieinfo-text">
        <h1>{movie.title}</h1>
        <h3>PLOT</h3>
        <p>{movie.overview}</p>
        <div className="rating-director">
          <div>
            <h3>IMDB RATING</h3>
            <div className="score">{movie.vote_average}</div>
          </div>
        </div>
      </div>
    </div>
    );
}

export default ModalContent;