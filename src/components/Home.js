import React, { useState } from 'react';
import { API_URL, 
    API_KEY, 
    POSTER_SIZE, 
    BACKDROP_SIZE, 
    IMAGE_BASE_URL, 
    POPULAR_BASE_URL, 
    SEARCH_BASE_URL
} from '../config';

// import elements
import HeroImage from './elements/HeroImage';
import LoadMore from './elements/LoadMore'; 
import Spinner from './elements/Spinner'; 
import SearchBar from './elements/SearchBar'; 
import MovieThumb from './elements/MovieThumb';
import Grid from './elements/Grid';
import Modal from './elements/Modal';
import NoImage from './images/no_image.jpg';

// custom hook
import { useHomeFetch } from './hooks/useHomeFetch';
import { useModal } from './hooks/useModal';


const Home = () => {

    const [{
        state : {movies, currentPage, totalPages, heroImage},
        loading, 
        error
    }, fetchMovies] = useHomeFetch();

    const [searchTerm, setSearchTerm] = useState('');

    //modal

    const {isShowing, toggle} = useModal(); 

    //load more callback
    const loadMoreMovies = () => {
        const searchEndpoint = `${SEARCH_BASE_URL}${searchTerm}&page=${currentPage + 1}`;
        const popularEndpoint = `${POPULAR_BASE_URL}&page=${currentPage + 1}`;

        const endpoint = searchTerm ? searchEndpoint : popularEndpoint; 

        fetchMovies(endpoint);
    }

    //search movies

    const searchMovies = search => {
        const endpoint = search ? SEARCH_BASE_URL + search : POPULAR_BASE_URL;

        setSearchTerm(search);
        fetchMovies(endpoint);
    }

    // check error/ loading
    if(error) return ("something went wrong")
    if (!movies[0]) return <Spinner />

    //rendering
    return (
    <>
        { !searchTerm && (
              <HeroImage 
              image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${heroImage.backdrop_path}`}
              title={heroImage.original_title}
              text={heroImage.overview}
              />
        )}
        <SearchBar callback={searchMovies} />
        <Grid header={searchTerm ? 'Search Reasult' : 'Popular Movies'}>
            {movies.map(movie => (
                <MovieThumb key={movie.id}
                clickable
                image={
                    movie.poster_path
                    ?`${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}`
                    : NoImage
                }
                movieId={movie.id}
                mobieName={movie.original_title}
                />
            ))}
            <Modal
            isShowing={isShowing}
            hide={toggle} 
            />
        </Grid>
        {loading && <Spinner />}
        {currentPage < totalPages && !loading &&(
             <LoadMore text="Load More" callback={loadMoreMovies} />
        )}
    </>
)}

export default Home;